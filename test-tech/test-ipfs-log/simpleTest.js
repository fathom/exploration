const IPFS = require('ipfs')
const Log  = require('ipfs-log')

const ipfs = new IPFS()
//  constructor (ipfs, id, entries, heads, clock, key, keys = [])
const log  = new Log(ipfs,'antoineslog',null,null,null)//key, keys = [])

//parameters
var message = process.argv[2];


ipfs.on('ready' , async () => {
	try{
	  console.log("----------------- IPFS IS READY --------------------------")
	  //first append 
	  await log.append('one')
	  console.log("after first append :")
	  console.log(log.values)

	  //hash of first log
	  var firstHash=log.values[0].hash
	  console.log("first Hash : ")
	  console.log(firstHash)
	  console.log("-------------------------------------------")

	  //second append
	  await log.append('two')
	  console.log("after second append :")
	  console.log(log.values)
	  console.log("-------------------------------------------")


	  //someone else creates another log
	  const log2  = new Log(ipfs,'otherlog',null,null,log.clock)
	  await log2.append('what about letters?')
	  //log1 answers
	  await log.append("a")
	  //log2 answers
	  await log2.append("b")
	  console.log("log2 spoke")
	  console.log(log2.values)
	  console.log("log1 spoke")
	  console.log(log.values)
	  console.log("-------------------------------------------")

	  //save log into multihash
	  var multihash= await log.toMultihash(ipfs, log)
	  console.log("log1 saved to hash : "+multihash)
	  console.log("-------------------------------------------")

	  //log2 fetches log1 from mulithash and joins logs
	  var logs1fromIPFS= await Log.fromMultihash(ipfs, multihash)
	  console.log("log1 fetched from ipfs by user2 :")
	  console.log(logs1fromIPFS.values)
	  console.log("-------------------------------------------")
	  await log2.join(logs1fromIPFS)
	  console.log("log2 joined messages from log1 into own log :")
	  console.log(log2.values)
	  console.log(log2.toString())
	  console.log("-------------------------------------------")
	  console.log('done')
	  process.exit(0)
	} catch (e) {
	    console.error(e)
	    process.exit(1)
	}
})

