# Test the ipfs-log module
## Building:
`npm i`


## Testing:
To see a simple example of how two users can share the same log using ipfs-log:  
Run the test with node `node simpleTest.js`

To see the same example but with a Keystore to sign messages:  
Run the test with node `node testWithKeyStore.js`

## Experimenting:
We can use the previous protocol and websockets to build a client/relayer network. 
The files are in the clientRelayerArchitecture folder. A "Simple Relayer" is a 
server that justs broadcasts whichever message it gets to the other clients.
For now it has a key to sign the messages it broadcasts, but a more sophisticated 
relayer could join the logs and keep the head in memory using IPFS.
The client is inspired from the testWithKeyStore, and instantiates a client with 
a number input to set ipfs memory file and swarm peer port.
The "prompt" module is used for chat client-side and socket.io for straight-forward 
implementation of the client/server logic.

* To start relayer on 8000: `node clientRelayerArchitecture/simpleRelayer.js`

* To start a client choose a port number, for example: `node clientRelayerArchitecture/client.js 4001`

## Saving and Sharing Experiments
By playing around with an example or a test, you might come up with a cool new use case! 
Please don't hesitate to fork this branch, add your file to the `/examples` folder, and create an MR.

### A Note on Linting:
When you're ready to share your example, please run your code through the Standard JS linter:  
```
npm run lint
```
This will check all JavaScript files in the current working directory and call attention to any code that needs fixing. Taking this step will help to speed your MR along by ensuring that your code plays nicely with the rest of our codebase.  

## What's next?
- let the relayers keep logs in memory and serve them on request
- organize conversations around tags
- restrict logs to lists of users to make private conversations