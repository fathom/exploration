const IPFS = require('ipfs')
const Log  = require('ipfs-log')

const Keystore =require('key-store')

const dataPath = './data'

const ipfs = new IPFS({repo: dataPath + '/ipfs'})


//parameters
var message = process.argv[2];


ipfs.on('ready' , async () => {
  const keystore = new Keystore('./keystore')
	try{
	  console.log("----------------- IPFS IS READY --------------------------\n")

	  	console.log("--------------------- CREATING IDENTITIES ----------------------")
	 	//load identities from key store
	 	console.log("creating key store ...")
	 	await Keystore.createStore('./wallets')
	 	console.log("loading key store ...")
		const store = await Keystore.loadStore('./wallets')
		var allWalletIDs = await store.getWalletIDs()

		console.log(`All available wallets: ${allWalletIDs.join(', ')}`)
	  	console.log("-------------------------------------------")

	  	//create an identity for user1
		await store.saveWallet('user1', 'password', { privateKey: 'super secret private key' })
		console.log(`Created a new wallet named 'user1'.`)

		const wallet1 = await store.readWallet('user1', 'password')
		console.log(`Stored private key: ${wallet1.privateKey}`)
	  console.log("-------------------------------------------")

	  	//create an identity for user2
		await store.saveWallet('user2', 'password2', { privateKey: 'super secret private key is back' })
		console.log(`Created a new wallet named 'user2'.`)

		const wallet2 = await store.readWallet('user2', 'password2')
		console.log(`Stored private key: ${wallet2.privateKey}`)
	  console.log("-------------------------------------------")

	  //relog wallet IDs
	  	console.log("loading wallet IDs... ")
		allWalletIDs = await store.getWalletIDs()

		console.log(`All available wallets: ${allWalletIDs.join(', ')}`)
	  	console.log("-------------------------------------------\n")
	  	console.log("-------------------------------------------")

	  //Start writing in logs
	  //  constructor (ipfs, id, entries, heads, clock, key, keys = [])
	  const log  = new Log(ipfs,'mylog',null,null,null, key1, [key1.getPublic('hex'), key2.getPublic('hex')])
	  //first append 
	  await log.append('one')
	  console.log("after first append :")
	  console.log(log.values)
	  console.log("-------------------- STARTING LOGS -----------------------")

	  //hash of first log
	  var firstHash=log.values[0].hash
	  console.log("first Hash : ")
	  console.log(firstHash)

	  //second append
	  await log.append('two')
	  console.log("after second append :")
	  console.log(log.values)
	  console.log("-------------------------------------------")


	  //someone else creates another log
	  const log2  = new Log(ipfs,'otherlog',null,null,null, key2, [key1.getPublic('hex'), key2.getPublic('hex')])
	  await log2.append('what about letters?')
	  //log1 answers
	  await log.append("a")
	  //log2 answers
	  await log2.append("b")
	  console.log("log2 spoke")
	  console.log(log2.values)
	  console.log("log1 spoke")
	  console.log(log.values)
	  console.log("-------------------------------------------")

	  //save log into multihash
	  var multihash= await log.toMultihash(ipfs, log)
	  console.log("log1 saved to hash : "+multihash)
	  console.log("-------------------------------------------")

	  //log2 fetches log1 from mulithash and joins logs
	  var logs1fromIPFS= await Log.fromMultihash(ipfs, multihash)
	  console.log("log1 fetched from ipfs :")
	  console.log(logs1fromIPFS.values)
	  await log2.join(logs1fromIPFS)
	  console.log("log2 joined messages from log1")
	  console.log(log2.values)
	  console.log(log2.toString())
	  console.log("-------------------------------------------")
	  console.log('done')
	  process.exit(0)
	} catch (e) {
	    console.error(e)
	    process.exit(1)
	}
})

