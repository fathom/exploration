var app = require('express')();
//const path = require('path');
var helmet = require('helmet');
var cors = require('cors')

var port =  process.env.PORT || 8000

var http = require('http').createServer(app)
var io = require('socket.io')(http)


app.all('*', function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'accept, content-type, x-parse-application-id, x-parse-rest-api-key, x-parse-session-token');
     // intercept OPTIONS method
    if ('OPTIONS' == req.method) {
      res.send(200);
    }
    else {
      next();
    }
});
app.set('port', port);
app.use(helmet());
app.use(cors());

//app.use('/', app.static(path.join(__dirname, '../static')))
require('./server.js').server(app)