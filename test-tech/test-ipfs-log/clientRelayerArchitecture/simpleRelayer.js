var app = require('express')();
//const path = require('path');
var helmet = require('helmet');
var cors = require('cors')

var port =  process.env.PORT || 8000

var http = require('http').createServer(app)
var io = require('socket.io')(http)


app.all('*', function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'accept, content-type, x-parse-application-id, x-parse-rest-api-key, x-parse-session-token');
     // intercept OPTIONS method
    if ('OPTIONS' == req.method) {
      res.send(200);
    }
    else {
      next();
    }
});
app.set('port', port);
app.use(helmet());
app.use(cors());

//what's this line for?
//app.use('/', app.static(path.join(__dirname, '../static')))

//server for the relayer :
    var http = require('http').createServer(app)
    var io = require('socket.io')(http)
    const Keystore = require('orbit-db-keystore')

      console.log("------------------- CREATE IDENTITY FOR RELAYER ------------------------")
        //instantiate 1 key for the two users
      const keystore = new Keystore('./keystore')

      let key1
      try {
        key1 = keystore.getKey('relayer1') || keystore.createKey('relayer1')
      } catch (e) {
        console.error(e)
      }
      console.log("New key created for the relayer : "+key1.getPublic('hex'))

    http.listen(app.get('port'), function() {
      console.log('------------------------- Express/socket server listening on port ' + app.get('port'));
            io.on('connection', function(socket) {
                socket.on("message",function(args){
                    try {
                        console.log("------------------ relayer received a message -------------------")
                        console.log(args)
                        var response=args
                        response.relayer=key1.getPublic('hex')
                        response.relayerSignature=keystore.sign(key1,args.multihash)
                        //broadcast function only sends message to other sockets
                        socket.broadcast.send(response)
                        console.log("-----broadcasted...")
                        //update log head
                        //this is where log head could be saved (ipfs,ipfs-log...)
                    }
                    catch(e){
                        console.log("error on socket message")
                    }
                })
            })
    })