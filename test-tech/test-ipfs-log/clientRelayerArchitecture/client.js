const IPFS = require('ipfs')
const Log  = require('ipfs-log')

const Keystore = require('orbit-db-keystore')

const dataPath = './data'

const env=process.argv[2];
const ipfs = new IPFS({
	repo: dataPath + '/ipfs/'+env,
	config: {
	    Addresses: {
	      Swarm: [
	         '/ip4/127.0.0.1/tcp/'+env
	       ]
	    }
	}
})

const io = require('socket.io-client')
const socket=io.connect('http://localhost:8000')

const prompt = require('prompt');

function printLogValues(logValues){
	if (logValues.length===0){
		console.log("-*-*-")
	} else {
		console.log("*\nID: "+logValues[0].id,
			"\nAddress: "+logValues[0].key.substring(1,5)+"..."+
			"\nMessage: "+logValues[0].payload
		)
		printLogValues(logValues.slice(1))
	}
}

ipfs.on('ready' , async () => {
	try{
	  console.log("----------------- IPFS IS READY --------------------------\n")

	  // Start the prompt
	  console.log('connect to prompt...')
	  prompt.start();
	  prompt.get(['username'],async function (err, result1) {

	  	  console.log("------------------- CREATE IDENTITY FOR CLIENT ------------------------")
		  //instantiate 1 key for the two users
		  const keystore = new Keystore('./keystore')
		  ipfs.keystore = keystore
		  let key1
		  try {
		    key1 = keystore.getKey(result1.username) || keystore.createKey(result1.username)
		  } catch (e) {
		    console.error(e)
		  }
		  console.log("New key created for "+result1.username+" : "+key1.getPublic('hex'))

		  //Start writing in logs
		  console.log("\n------------------- STARTING LOG ------------------------")

		  //  constructor (ipfs, id, entries, heads, clock, key, keys = []) "*" opens this log to all addresses
		  const log  = new Log(ipfs,result1.username,null,null,null, key1, ["*"])
		  console.log("log created")

		  //set listener for incoming messages (log in console and append to current log)
		  socket.on('message',async (message)=>{
		  	console.log("MESSAGE")
	  		var logFromRelayer= await Log.fromMultihash(ipfs, message.multihash)
		  	await log.join(logFromRelayer)
			console.log("------------- log after joining incoming log :")
			console.log(log.toString())
			printLogValues(log.values)
			console.log("------------------------------------")
		  })

		  //start the message prompt
		  async function newMessage (log,key,id){
			  prompt.get(['message'],async function (err, result) {
			  	if (result.message=="exit"){
		  			process.exit(0)
			  	} else {
		  			console.log("\n------------------- new message ------------------")
		  			//append message to current log
					await log.append(result.message)
					console.log("------------- log after new append :")
					console.log(log.toString())
					printLogValues(log.values)
					console.log("------------------------------------")
					//send new hash to relayer
		  			var multihash= await log.toMultihash(ipfs, log)
					await socket.send({multihash:multihash,client:key.getPublic('hex'),id:id})
				    console.log('message sent');
				    newMessage(log,key)
			  	}
			  });
		  }
	  	  newMessage(log,key1,result1.username)
	  })
	} catch (e) {
	    console.error(e)
	    process.exit(1)
	}
})

