module.exports.server = function(app) {
    var http = require('http').createServer(app)
    var io = require('socket.io')(http)
    const Keystore = require('orbit-db-keystore')

      console.log("------------------- CREATE IDENTITY FOR RELAYER ------------------------")
        //instantiate 1 key for the two users
      const keystore = new Keystore('./keystore')

      let key1
      try {
        key1 = keystore.getKey('relayer1') || keystore.createKey('relayer1')
      } catch (e) {
        console.error(e)
      }
      console.log("New key created for the relayer : "+key1.getPublic('hex'))

    http.listen(app.get('port'), function() {
      console.log('------------------------- Express/socket server listening on port ' + app.get('port'));
            io.on('connection', function(socket) {
                socket.on("message",function(args){
                    try {
                        console.log("------------------ relayer received a message -------------------")
                        console.log(args)
                        var response=args
                        response.relayer=key1.getPublic('hex')
                        response.relayerSignature=keystore.sign(key1,args.multihash)
                        io.sockets.send(response)
                        console.log("-----broadcasted...")
                        //update log head
                        //this is where log head could be saved (ipfs,ipfs-log...)
                    }
                    catch(e){
                        console.log("error on socket message")
                    }
                })
            })
    })
}
