const IPFS = require('ipfs')
const Log  = require('ipfs-log')

const Keystore = require('orbit-db-keystore')

const dataPath = './data'

const ipfs = new IPFS({repo: dataPath + '/ipfs'})


//parameters
var message = process.argv[2];


ipfs.on('ready' , async () => {

	try{
	  console.log("----------------- IPFS IS READY --------------------------\n")

	  	console.log("------------------- CREATE IDENTITIES ------------------------")
		//instantiate 2 keys for the two users
	  const keystore = new Keystore('./keystore')
	  ipfs.keystore = keystore

	  let key1, key2
	  try {
	    key1 = keystore.getKey('mylog') || keystore.createKey('mylog')
	    key2 = keystore.getKey('theirlog') || keystore.createKey('theirlog')
	  } catch (e) {
	    console.error(e)
	  }
	  console.log("New key created for myself : "+key1.getPublic('hex'))
	  console.log("New key created for them : "+key2.getPublic('hex'))
	  	console.log("\n------------------- STARTING LOGS ------------------------")

	  //Start writing in logs
	  //  constructor (ipfs, id, entries, heads, clock, key, keys = [])
	  const log  = new Log(ipfs,'mylog',null,null,null, key1, [key1.getPublic('hex'), key2.getPublic('hex')])
	  //first append 
	  await log.append('one')
	  console.log("after first append :")
	  console.log(log.values)
	  console.log("-------------------------------------------")

	  //hash of first log
	  var firstHash=log.values[0].hash
	  console.log("first Hash : ")
	  console.log(firstHash)

	  //second append
	  await log.append('two')
	  console.log("after second append :")
	  console.log(log.values)
	  console.log("-------------------------------------------")


	  //someone else creates another log
	  const log2  = new Log(ipfs,'theirlog',null,null,null, key2, [key1.getPublic('hex'), key2.getPublic('hex')])
	  await log2.append('what about letters?')
	  //log1 answers
	  await log.append("a")
	  //log2 answers
	  await log2.append("b")
	  console.log("log2 spoke")
	  console.log(log2.values)
	  console.log("log1 spoke")
	  console.log(log.values)
	  console.log("-------------------------------------------")

	  //save log into multihash
	  var multihash= await log.toMultihash(ipfs, log)
	  console.log("log1 saved to hash : "+multihash)
	  console.log("-------------------------------------------")

	  //log2 fetches log1 from mulithash and joins logs
	  var logs1fromIPFS= await Log.fromMultihash(ipfs, multihash)
	  console.log("log1 fetched from ipfs :")
	  console.log(logs1fromIPFS.values)
	  await log2.join(logs1fromIPFS)
	  console.log("log2 joined messages from log1")
	  console.log(log2.values)
	  console.log(log2.toString())
	  console.log("-------------------------------------------")
	  console.log('done')
	  process.exit(0)
	} catch (e) {
	    console.error(e)
	    process.exit(1)
	}
})

