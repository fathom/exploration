// this test uses prompt to
// -change username
// -chat with a list of people (defaults to antoine)

function close (sbot) {
  console.log('closing...')
  sbot.close()
  process.exit()
}

const ssbClient = require('ssb-client')
const prompt = require('prompt')
const pull = require('pull-stream')

// first one is Antoine
let listOfPeopleInThePrivateChat = ['@0hhzPDSecYeruTPKf4lFQVN0k6e0yNOi7MjUf96ieaQ=.ed25519', '@pxNIuK0letN977bP6RDL/pV5d1IU/1fx8wTm2sr1x8U=.ed25519']
let id

// this function focuses on publishing a private message to the list of people in the private chat
function postPrivateMessage (sbot, prompt) {
  console.log("ENTER MESSAGE BELOW             -(type 'exit' to stop chat)-")
		    prompt.get(['message'], async function (err, resp) {
		    	if (resp.message === 'exit') {
		    		close(sbot)
		    	} else {
		    		let recps = listOfPeopleInThePrivateChat.map((id) => { return {link: id} })
		    		console.log('resp.message')
		    		console.log(resp.message)
		    		console.log('listOfPeopleInThePrivateChat')
		    		console.log(listOfPeopleInThePrivateChat)
		    		var obj = {
					  type: 'post',
					  text: resp.message,
					  recps: recps
      }
		    		console.log(obj)
				  	sbot.private.publish({
					  type: 'post',
					  text: resp.message,
					  recps: recps
      }, listOfPeopleInThePrivateChat,
					 function (err, msg) {
					 	if (err) { throw err }
        pull(
						  sbot.createUserStream({ id: id }),
						  pull.collect(function (err, msgs) {
						    if (err) { throw err }
						  	// filter posts
						  	msgs.forEach(function (msg) {
						  		if (typeof msg.value.content === 'string') {
							  		console.log('\n' + msg.value.content)
							  		sbot.private.unbox(msg.value.content, function (err, decrypted) {
							  			console.log('MESSAGE')
							  			if (decrypted.type == 'post') {
							  				console.log(decrypted.text)
							  			}
							  		})
						  		}
						  	})

						    postPrivateMessage(sbot, prompt)
						  })
        )
      })
		    	}
  })
}

// this function looks into the message history for the current nickname of the user
function getName (sbot, userId, cb) {
  pull(
	  sbot.links({
	    source: userId,
	    dest: userId,
	    rel: 'about',
	    values: true
	  }),
	  pull.collect(function (err, msgs) {
	  	function lookForName (msgs) {
	  		if (msgs.length === 0) {
	  			return 'noNickName'
	  		} else {
	  			if (msgs[msgs.length - 1].value.content.name) {
	  				return msgs[msgs.length - 1].value.content.name
	  			} else {
	  				msgs.pop()
	  				lookForName(msgs)
	  			}
	  		}
	  	}
	  	let name = lookForName(msgs)
	  	cb(name)
	  })
  )
}

// main function : starts with instanciation of client
ssbClient(function (err, sbot) {
  if (err) { throw err }

  // sbot is now ready. when done:
  console.log('sbot ready !')
  try {
  	// fetch id
  	sbot.whoami(function (err, info) {
	    console.log('Your client id is ' + info.id)
	    id = info.id
	    // add id to private chat
	    listOfPeopleInThePrivateChat.push(info.id)
	    console.log('id added to private chat list')
	    // get current nickname
	    getName(sbot, info.id, function (name) {
	    	console.log('Your current name is +++ ' + name + ' +++')
	  	  // Start the prompt
		  console.log('connect to prompt...')
		  prompt.start()
		  // use prompt to ask for username change
		  console.log('Do you want to change your name ?')
		  prompt.get(['yes_no'], async function (err, resp) {
			  if (resp.yes_no === 'yes') {
				  prompt.get(['username'], async function (err, resp) {
					  // set name
					  sbot.publish({
						  type: 'about',
						  about: id,
						  name: resp.username
					  }, function (err) {
					  	console.log('you just named your profile ' + resp.username)
					  	postPrivateMessage(sbot, prompt)
					  })
				  })
          } else {
            postPrivateMessage(sbot, prompt)
          }
		  })
	    })
    })
  } catch (e) {
    console.error(e)
  }
})
