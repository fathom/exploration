# Testing and using Secure ScuttleButt libraries for JS

**WARNING: This will affect and post on your current ssb account if you have one !**
**Run the second test to avoid making public changes **

## Install:

- First run `npm i` to install modules
- If you don't have ssb installed on your computer yet, run 'npm install scuttlebot -g' to create an identity (i.e. a key pair) for you and store it under .ssb/secret
- Then launch your ssb server by running `sbot server`. This can take a little while.

## Test:

1. To see a simple test of ssb functionalities, run `node simpleTest.js`.
- log your client ID
- change your nickname to whiterabbit
- publish a public Hello World message
- display your message feed (2 last messages)

2. To run a test with only private messages: 
- run 'sbot plugins.install ssb-private'
- run 'node testPrivateMessage.js'
This test is using the 'prompt' module to enable cli interactions. It will first ask you if you want to rename your profile, and then you'll start chating with the only other participant (me, @joelamouche).
Change the listOfPeopleInThePrivateChat variable to add/remove people to your private chat.

## Next:

- Improve chat UX by at least putting the names of the users

- Add/remove people from the chat with the cli

- Check out APIs and modules at http://scuttlebot.io/apis/scuttlebot/ssb.html

## More ressources

https://ssbc.github.io/scuttlebutt-protocol-guide/
https://ssbc.github.io/scuttlebot/
