function close (sbot) {
  console.log('closing...')
  sbot.close()
  process.exit()
}

var ssbClient = require('ssb-client')
ssbClient(function (err, sbot) {
  if (err) { throw err }

  // sbot is now ready. when done:
  console.log('sbot ready !')
  try {
  	// fetch id
  	sbot.whoami(function (err, info) {
	  console.log('Your client id is ' + info.id)
	  var id = info.id
	  // set name
	  sbot.publish({
		  type: 'about',
		  about: id,
		  name: 'whiterabbit'
	  }, function (err) {
	  	console.log('you just named your profile whiterabbit')
	  	// say hello
	  	sbot.publish({
		  type: 'post',
		  text: 'Hello, world!'
        }, function (err, msg) {
		  console.log('you just published a hello message')
		  // read messages
		  var pull = require('pull-stream')
          pull(
			  sbot.createUserStream({ id: id }),
			  pull.collect(function (err, msgs) {
			    if (err) { throw err }
			    console.log('number of messages in the feed : ' + msgs.length)
			    console.log('last 2 messages of the feed : ')
			    console.log(msgs[msgs.length - 2].value.content)
			    console.log(msgs[msgs.length - 1].value.content)
			    close(sbot)
			  })
          )
        })
	  })
    })
  } catch (e) {
    console.error(e)
  }
})
