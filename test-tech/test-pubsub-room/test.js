//to use this test, enter node test.js <number>
//where <number> is a 4digit number like 1332 or 1331 used for tcp/swarm communication
//console will show when new users join or leave room, and will send an incremental message evry 3 seconds

const Room = require('ipfs-pubsub-room')
const IPFS = require('ipfs')
const env=process.argv[2];
const ipfs = new IPFS({
  repo:repo(),
  EXPERIMENTAL: {
    pubsub: true
  },
  config: {
    Addresses: {
      Swarm: [
         '/ip4/127.0.0.1/tcp/'+env
       ]
    }
  }
})

function repo (){
  return "ipfs/test/"+env
}

// IPFS node is ready, so we can start using ipfs-pubsub-room
ipfs.on('ready', () => {
  const room = Room(ipfs, 'ze room')

  room.on('peer joined', (peer) => {
    console.log('Peer joined the room', peer)
  })

  room.on('peer left', (peer) => {
    console.log('Peer left...', peer)
  })

  // now started to listen to room
  room.on('subscribed', () => {
    console.log('Now connected!')
    var count=1
    setInterval(()=>{
      room.broadcast(count+" metre à pied, ca use ca use, ca use les souliers")
      count++
    },3000)
  })

  room.on('message', (message) => {
    console.log("*"+message.from+"*\n"+message.data+"\n")
  })
})

