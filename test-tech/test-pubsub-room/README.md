# Test the ipfs-pubsub-room module

## Building:
`npm i`


## Testing:
To see a simple example of how one can use ipfs-pubsub-room to create an ipfs based p2p room:  
Run the test with node `node test.js 1331` or replace 1331 with another port (for swarm port and ipfs file ref).

The test will use ipfs and ipfs-pubsub-room to instantiate a client connected to 
a room named 'ze room' and will listen to events such as user presence or incoming messages.
The client then starts publishing a message every 3 seconds.

The next step to improve this client could be to add the 'prompt' module to allow live chat.

## Saving and Sharing Experiments:
By playing around with an example or a test, you might come up with a cool new use case!   
Please don't hesitate to fork this branch, add your own file to the `/examples` folder, and create an MR.

### A Note on Linting:
When you're ready to share your example, please run your code through the Standard JS linter:  
```
npm run lint
```
This will check all JavaScript files in the current working directory and call attention to any code that needs fixing. Taking this step will help to speed your MR along by ensuring that your code plays nicely with the rest of our codebase.  
