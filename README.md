# Exploration

This repository contains the exploration of different technologies to help the building 
of Fathom tools.

In the spirit of constructionist learning, those tests and examples can be used 
as a playground to understand and share tools. Furthermore, this 
is a space where coders/builders/learners can compare/benchmark technologies and libraries. 

## Exploration Guidelines

Here is the suggested best practice to explore libraries and technologies in this repo:
- clone this repo
- create a branch named after the tech your exploring and your name
- run the tests to get a first understanding of the tool you're exploring
- copy a test or an example in the /examples folder specific to each exploration folder
- play around with the code to be sure you understand the functionalities of the tool
- if you modified your test/example enough that people might learn from it or that it could be used in the more general Fathom Architecture, push your branch and make a MR for us to review. If you do, please make sure to add a README.md to your folder with instructions, analysis or questions. Please add a short section with your MOTIVATION and APPROACH section in this README.md (e.g. check out the ones below)

## Technologies and Libraries

### P2P Decentralized Messaging Protocols

*For now, the tests and examples are all centered around P2P Decentralized 
Messaging Protocols.*

**MOTIVATION** : We want a secure way for our users to chat around concepts that doesn't rely on 
a central server (that could be used to censor or temper information). This communication infrastructure, if reliable, could also be used to commincate important data such as claims.

**APPROACH** : We decided to start exploring ipfs based communication solutions. They came naturally as ipfs is the canonical data storage counterpart to ethereum. The next step will be to test the more mature SSB tool ecosystem. Tests are within the [test-tech directory](https://gitlab.com/fathom/exploration/tree/tests-ipfs-log-and-ipfs-pubsub-room/test-tech):

#### ipfs-log: 
- [ipfs-log](https://github.com/orbitdb/ipfs-log) is an immutable, operation-based conflict-free replicated data structure (CRDT) for distributed systems. It's an append-only log that can be used to model a mutable, shared state between peers in p2p applications.
- [Select this folder](https://gitlab.com/fathom/exploration/tree/tests-ipfs-log-and-ipfs-pubsub-room/test-tech/test-ipfs-log) and follow instructions to learn how to turn this powerful tool into an ipfs based chat client.

#### ipfs-pubsub-room: 
- [ipfs-pubsub-room](https://github.com/ipfs-shipyard/ipfs-pubsub-room) creates a room based on an IPFS pub-sub channel. Emits membership events, listens for messages, broadcast and direct messeges to peers
- [Select this folder](https://gitlab.com/fathom/exploration/tree/tests-ipfs-log-and-ipfs-pubsub-room/test-tech/test-pubsub-room) and follow instructions to learn how to turn this powerful tool into an ipfs based chat client.

**CONCLUSION (COMPARISON)**: My humble opinion is that ipfs-pubsub-room is easier and more straight forward to use, but definitely doesn't let users use all the functionalities offered by ipfs-log.
ipfs-pubsub-room is probably implmented using ipfs-log and sockets.
