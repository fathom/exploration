`FEATURE` Proposal
<!---
* NOTE: This form is for submitting feature proposals.
  (If you are reporting a Bug, please use the Bug template instead)
* Please provide a concise description of the issue in the Title above.
--->
--------------------------------------------------------------------------------
## Current Behavior
<!-- Describe the current behavior or state of things (without this feature) -->


## Expected Behavior 
<!-- Describe the expected behavior or state of things (with this feature implemented) -->


## Motivation and Context
<!-- Provide context (e.g., examples, use cases, user stories, goals) -->
<!-- How has this issue affected you? What are you trying to accomplish? --> 
<!-- Providing specific context helps us come up with a useful solution. -->


## Possible Implementation
<!-- Suggest a solution -- describe your proposed change + any key implementation details -->


## Related Issues
<!-- Reference open issues by their issue number --> 

  * **Depends On:**  
  <!-- Are there any dependent issues that should be completed BEFORE addressing this one? -->
  <!-- If so, please list them here as checklist items (e.g., "- [ ] Do this first") for easier tracking -->
  <!-- If any of these items have not yet been documented in an open issue, please open up an issue for it :-) -->
  <!-- A starter item is provided, below. You may add to it, or delete if not needed -->

- [ ] 

  * **Enables:**  
  <!-- List any issues that could/should be addressed AFTER completing this one --> 
  

## Info, Links, etc
<!-- Links: Post links to helpful info, resources -->
<!-- Images: Drag screenshots or other image files here -->
<!-- Code: Format any relevant code, commands, output, using code blocks (```) -->


## Notify Others
<!-- Notify relevant individuals by adding their GitLab handles here -->


--------------------------------------------------------------------------------
/label ~"feature"

<!-- Add further specification using the drop-down lists below, as appropriate -->

